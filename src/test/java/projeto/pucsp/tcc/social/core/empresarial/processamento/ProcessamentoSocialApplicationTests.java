package projeto.pucsp.tcc.social.core.empresarial.processamento;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import projeto.pucsp.tcc.social.core.empresarial.processamento.cliente.ClienteConcluirLance;
import projeto.pucsp.tcc.social.core.empresarial.processamento.cliente.implementacao.ClienteConcluirLanceRabbitImpl;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Localizacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Oferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.observador.LanceObservador;
import projeto.pucsp.tcc.social.core.empresarial.processamento.propriedade.PropriedadeConcluirLance;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio.OfertaRepositorio;
import projeto.pucsp.tcc.social.core.empresarial.processamento.servico.LanceServico;
import projeto.pucsp.tcc.social.core.empresarial.processamento.servico.OfertaServico;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProcessamentoSocialApplicationTests {


    private static final String LATITUDE = "-45.23424432";
    private static final String LONGITUDE = "-23.4364778";
    private static final String REPOSITORIO_DEVE_RETORNAR_NADA = "repositorio deve retornar nada";

    @Autowired
    private LanceObservador lanceObservador;

    @Autowired
    private OfertaRepositorio repositorio;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private PropriedadeConcluirLance propriedadeConcluirLance;

    @Mock
    private OfertaRepositorio repositorioMock;

    private LanceObservador lanceObservadorMock;

    @Before
    public void setup() {

        OfertaRecurso ofertaRecurso = new OfertaServico(repositorioMock);

        ClienteConcluirLance clienteConcluirLance = new ClienteConcluirLanceRabbitImpl(rabbitTemplate, propriedadeConcluirLance);

        final LanceRecurso lanceServico = new LanceServico(ofertaRecurso, clienteConcluirLance);

        lanceObservadorMock = new LanceObservador(lanceServico);

    }

    @Test
    public void processarOfertaComLanceDisponivel() {

        final LancePublicacao lancePublicacao = new LancePublicacao();

        lancePublicacao.setAlimentoId(2);
        lancePublicacao.setSocialId(1);
        lancePublicacao.setLanceId(1);
        lancePublicacao.setQuantidadeAlimento(4);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        lancePublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterOfertas(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterOferta(lancePublicacao, 1)));

        Mockito
                .when(repositorioMock.atualizarOferta(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        lanceObservadorMock.processar(lancePublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComLanceDisponivelSemPossibilidadeDeAtualizar() {

        final LancePublicacao lancePublicacao = new LancePublicacao();

        lancePublicacao.setAlimentoId(2);
        lancePublicacao.setSocialId(1);
        lancePublicacao.setLanceId(1);
        lancePublicacao.setQuantidadeAlimento(4);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        lancePublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterOfertas(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterOferta(lancePublicacao, 1)));

        Mockito
                .when(repositorioMock.atualizarOferta(anyInt(), anyInt(), anyInt()))
                .thenReturn(0);

        lanceObservadorMock.processar(lancePublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComMaisLancesDoQueOferta() {

        final LancePublicacao lancePublicacao = new LancePublicacao();

        lancePublicacao.setAlimentoId(2);
        lancePublicacao.setSocialId(1);
        lancePublicacao.setLanceId(1);
        lancePublicacao.setQuantidadeAlimento(1);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        lancePublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterOfertas(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterOferta(lancePublicacao, 1), obterOferta(lancePublicacao, 2)));

        Mockito
                .when(repositorioMock.atualizarOferta(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        lanceObservadorMock.processar(lancePublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaComMenosLancesDoQueOfertaNecessita() {

        final LancePublicacao lancePublicacao = new LancePublicacao();

        lancePublicacao.setAlimentoId(2);
        lancePublicacao.setSocialId(1);
        lancePublicacao.setLanceId(1);
        lancePublicacao.setQuantidadeAlimento(300);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        lancePublicacao.setLocalizacao(localizacao);

        Mockito.when(
                repositorioMock
                        .obterOfertas(anyInt(), any(BigDecimal.class), any(BigDecimal.class)))
                .thenReturn(List.of(obterOferta(lancePublicacao, 1), obterOferta(lancePublicacao, 2)));

        Mockito
                .when(repositorioMock.atualizarOferta(anyInt(), anyInt(), anyInt()))
                .thenReturn(1);

        lancePublicacao.setQuantidadeAlimento(1);

        lanceObservadorMock.processar(lancePublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    @Test
    public void processarOfertaSemLancesDisponiveis() {

        final LancePublicacao lancePublicacao = new LancePublicacao();

        lancePublicacao.setAlimentoId(2);
        lancePublicacao.setSocialId(1);
        lancePublicacao.setLanceId(1);
        lancePublicacao.setQuantidadeAlimento(1);

        final Localizacao localizacao = new Localizacao();

        localizacao.setLatitude(LATITUDE);
        localizacao.setLongitude(LONGITUDE);

        lancePublicacao.setLocalizacao(localizacao);

        lanceObservador.processar(lancePublicacao);

        Assert.assertTrue(REPOSITORIO_DEVE_RETORNAR_NADA, repositorio.findAll().isEmpty());

    }

    private static Oferta obterOferta(LancePublicacao lancePublicacao, Integer idLance) {

        final Oferta oferta = new Oferta();

        oferta.setId(idLance);

        oferta.setSituacaoOferta(0);

        oferta.setLoteFk(1);

        oferta.setQuantidadeAlimento(lancePublicacao.getQuantidadeAlimento());

        return oferta;

    }
}
