package projeto.pucsp.tcc.social.core.empresarial.processamento.cliente.implementacao;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.cliente.ClienteConcluirLance;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LanceCompensado;
import projeto.pucsp.tcc.social.core.empresarial.processamento.propriedade.PropriedadeConcluirLance;

@Component
public class ClienteConcluirLanceRabbitImpl implements ClienteConcluirLance {

    private final RabbitTemplate rabbitTemplate;

    private final PropriedadeConcluirLance propriedadeConcluirLance;

    public ClienteConcluirLanceRabbitImpl(RabbitTemplate rabbitTemplate, PropriedadeConcluirLance propriedadeConcluirLance) {
        this.rabbitTemplate = rabbitTemplate;
        this.propriedadeConcluirLance = propriedadeConcluirLance;
    }

    @Override
    public void publicar(LanceCompensado lanceCompensado) {

        rabbitTemplate.convertAndSend(propriedadeConcluirLance.getTopicExchange(), lanceCompensado);

    }
}

