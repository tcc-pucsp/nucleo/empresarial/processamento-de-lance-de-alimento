package projeto.pucsp.tcc.social.core.empresarial.processamento.propriedade;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "rabbit.concluir.lance")
public class PropriedadeConcluirLance {

    private String topicExchange;

}
