package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empresa")
@Data
public class Empresa {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "endereco_fk")
    private Integer enderecoFk;

    @Column(name = "cnpj_fk")
    private Integer cnpjFk;

    @Column(name = "nomeEmpresa")
    private String nomeEmpresa;

    @Column(name = "razao_social")
    private String razaoSocial;

    @Column(name = "nome_fantasia")
    private String nomeFantasia;

    @Column(name = "email")
    private String email;

}
