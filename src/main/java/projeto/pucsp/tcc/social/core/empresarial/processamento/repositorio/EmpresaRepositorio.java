package projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Empresa;

public interface EmpresaRepositorio extends JpaRepository<Empresa, Integer> {
}
