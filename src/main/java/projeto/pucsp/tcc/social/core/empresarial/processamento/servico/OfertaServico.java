package projeto.pucsp.tcc.social.core.empresarial.processamento.servico;

import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Localizacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Oferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.OfertaRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio.OfertaRepositorio;

import java.math.BigDecimal;
import java.util.List;

@Service
public class OfertaServico implements OfertaRecurso {

    private final OfertaRepositorio repositorio;

    public OfertaServico(OfertaRepositorio repositorio) {
        this.repositorio = repositorio;
    }

    @Override
    public List<Oferta> obterOfertas(Integer alimentoId, Localizacao localizacao) {
        return repositorio
                .obterOfertas(alimentoId, new BigDecimal(localizacao.getLatitude()), new BigDecimal(localizacao.getLongitude()));
    }

    @Override
    public Boolean atualizar(Oferta oferta) {
        return repositorio.
                atualizarOferta(oferta.getId(), oferta.getSituacaoOferta(), oferta.getQuantidadeAlimentoCompensado())
                .equals(1);
    }

}

