package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

@Data
public class Localizacao {

    private String latitude;

    private String longitude;

}
