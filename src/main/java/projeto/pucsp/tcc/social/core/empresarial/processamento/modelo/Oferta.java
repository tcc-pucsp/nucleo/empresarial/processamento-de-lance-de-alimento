package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "oferta")
@Data
public class Oferta {

    private static final Integer OFERTA_COMPENSANDA_POR_COMPLETO = 2;

    private static final Integer OFERTA_COMPENSANDA_PARCIALMENTE = 1;

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "empresa_fk")
    private Integer empresaFk;

    @Column(name = "situacao_oferta")
    private Integer situacaoOferta;

    @Column(name = "quantidade_alimento")
    private Integer quantidadeAlimento;

    @Column(name = "lote_fk")
    private Integer loteFk;

    @Transient
    private Integer quantidadeAlimentoCompensado;

    public void compensar(Integer quantidadeAlimento) {

        if (calcularVariacaoCompensamento(quantidadeAlimento) >= 0) {

            quantidadeAlimentoCompensado = compensar();

        } else {

            this.quantidadeAlimento -= quantidadeAlimento;

            situacaoOferta = OFERTA_COMPENSANDA_PARCIALMENTE;

            quantidadeAlimentoCompensado = quantidadeAlimento;

        }

    }

    private Integer compensar() {

        final Integer quantidadeAlimentoAnterior = this.quantidadeAlimento;

        situacaoOferta = OFERTA_COMPENSANDA_POR_COMPLETO;

        this.quantidadeAlimento = 0;

        return quantidadeAlimentoAnterior;

    }

    private Integer calcularVariacaoCompensamento(Integer quantidadeAlimento) {
        return quantidadeAlimento - this.quantidadeAlimento;
    }

}
