package projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Lote;

public interface LoteRepositorio extends JpaRepository<Lote, Integer> {
}
