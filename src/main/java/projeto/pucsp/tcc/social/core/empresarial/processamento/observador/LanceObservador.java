package projeto.pucsp.tcc.social.core.empresarial.processamento.observador;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.LanceRecurso;

@Slf4j
@Component
public class LanceObservador implements LanceRecurso {

    private final LanceRecurso servico;

    public LanceObservador(@Qualifier("lanceServico") LanceRecurso servico) {
        this.servico = servico;
    }

    @Override
    @RabbitListener(queues = "processar.lance")
    public void processar(LancePublicacao lancePublicacao) {

        log.info("Processar Lance  : {}", lancePublicacao);

        servico.processar(lancePublicacao);

    }

}
