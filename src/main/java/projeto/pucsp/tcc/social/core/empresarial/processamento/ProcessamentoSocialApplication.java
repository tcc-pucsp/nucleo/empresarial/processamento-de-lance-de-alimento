package projeto.pucsp.tcc.social.core.empresarial.processamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import projeto.pucsp.tcc.social.core.empresarial.processamento.propriedade.PropriedadeConcluirLance;

@SpringBootApplication
@EnableConfigurationProperties(PropriedadeConcluirLance.class)
public class ProcessamentoSocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcessamentoSocialApplication.class, args);
	}

}
