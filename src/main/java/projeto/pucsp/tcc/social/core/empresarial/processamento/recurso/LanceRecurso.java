package projeto.pucsp.tcc.social.core.empresarial.processamento.recurso;

import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LancePublicacao;

public interface LanceRecurso {

    void processar(LancePublicacao lancePublicacao);

}
