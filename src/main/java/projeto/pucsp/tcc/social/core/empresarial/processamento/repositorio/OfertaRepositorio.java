package projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Oferta;

import java.math.BigDecimal;
import java.util.List;

public interface OfertaRepositorio extends JpaRepository<Oferta, Integer> {

    /**
     * @param alimentoId identificador de alimento
     * @param latitude   latitude em BigDecimal
     * @param longitude  longitutde em BigDecimal
     * @return lista de Lance Disponivel para uma distancia menor que 10km
     * @apiNote A fórmula de Haversine é uma importante equação usada em navegação,
     * fornecendo distâncias entre dois pontos de uma esfera a partir de suas latitudes e longitudes.
     * É um caso especial de uma fórmula mais geral de trigonometria esférica, a lei dos Haversines,
     * relacionando os lados a ângulos de uma esfera "triangular".
     * https://pt.wikipedia.org/wiki/F%C3%B3rmula_de_Haversine
     */
    @Query(value = "select o.*,\n" +
            "       (6371 *\n" +
            "        acos(\n" +
            "                        cos(radians(:latitude)) *\n" +
            "                        cos(radians(e.latitude)) *\n" +
            "                        cos(radians(:longitude) - radians(e.longitude)) +\n" +
            "                        sin(radians(:latitude)) *\n" +
            "                        sin(radians(e.latitude))\n" +
            "            )) as distancia\n" +
            "from oferta o\n" +
            "         inner join lote l on o.lote_fk = l.id " +
            "         inner join empresa emp on o.empresa_fk = emp.id\n" +
            "         inner join endereco e on emp.endereco_fk = e.id\n" +
            "where l.alimento_id = :alimento_id and o.situacao_oferta <> 2 and l.data_validade > (now() + 2)\n" +
            "group by o.id \n" +
            "having ABS(distancia) <= 10\n", nativeQuery = true)
    List<Oferta> obterOfertas(
            @Param("alimento_id") Integer alimentoId,
            @Param("latitude") BigDecimal latitude,
            @Param("longitude") BigDecimal longitude);

    @Modifying
    @Transactional
    @Query("update Oferta set situacaoOferta = :situacaoOferta, quantidadeAlimento = quantidadeAlimento - :quantidadeAlimento " +
            "where situacaoOferta <> 2 and " +
            "id = :id and " +
            "(quantidadeAlimento - :quantidadeAlimento) >= 0")
    Integer atualizarOferta(
            @Param("id") Integer id,
            @Param("situacaoOferta") Integer situacaoOferta,
            @Param("quantidadeAlimento") Integer quantidadeAlimento);

}
