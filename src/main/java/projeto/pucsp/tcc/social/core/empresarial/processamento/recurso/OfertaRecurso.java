package projeto.pucsp.tcc.social.core.empresarial.processamento.recurso;

import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Localizacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Oferta;

import java.util.List;

public interface OfertaRecurso {

    List<Oferta> obterOfertas(Integer alimentoId, Localizacao localizacao);

    Boolean atualizar(Oferta oferta);

}
