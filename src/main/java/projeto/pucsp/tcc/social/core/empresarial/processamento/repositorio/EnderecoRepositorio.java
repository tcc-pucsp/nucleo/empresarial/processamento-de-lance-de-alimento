package projeto.pucsp.tcc.social.core.empresarial.processamento.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Endereco;

public interface EnderecoRepositorio extends JpaRepository<Endereco, Integer> {
}
