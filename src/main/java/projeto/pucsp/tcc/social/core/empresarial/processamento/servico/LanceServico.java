package projeto.pucsp.tcc.social.core.empresarial.processamento.servico;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import projeto.pucsp.tcc.social.core.empresarial.processamento.cliente.ClienteConcluirLance;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LanceCompensado;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LancePublicacao;
import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.Oferta;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.LanceRecurso;
import projeto.pucsp.tcc.social.core.empresarial.processamento.recurso.OfertaRecurso;

import java.util.Iterator;
import java.util.List;

@Slf4j
@Service
public class LanceServico implements LanceRecurso {

    private final OfertaRecurso ofertaRecurso;

    private final ClienteConcluirLance clienteConcluirLance;

    public LanceServico(OfertaRecurso ofertaRecurso, ClienteConcluirLance clienteConcluirLance) {
        this.ofertaRecurso = ofertaRecurso;
        this.clienteConcluirLance = clienteConcluirLance;
    }

    @Override
    public void processar(LancePublicacao lancePublicacao) {

        log.info("Processando em base de dados ofertas Disponiveis para lance com id : {}", lancePublicacao.getLanceId());

        final List<Oferta> ofertas = ofertaRecurso
                .obterOfertas(
                        lancePublicacao.getAlimentoId(),
                        lancePublicacao.getLocalizacao());

        if (ofertas.isEmpty()) {

            log.info("Não existe oferta para compensar a lance {}", lancePublicacao);

            return;

        }

        compensarOfertas(lancePublicacao, ofertas);

    }

    private void compensarOfertas(LancePublicacao lancePublicacao, List<Oferta> ofertas) {

        final Iterator<Oferta> iterator = ofertas.iterator();

        Integer quantidadeAlimento = lancePublicacao
                .getQuantidadeAlimento();

        while (quantidadeAlimento > 0 && iterator.hasNext()) {

            final Oferta oferta = iterator.next();

            log.info("{}", oferta);

            oferta.compensar(quantidadeAlimento);

            final boolean atualizado = ofertaRecurso
                    .atualizar(oferta);

            if (atualizado) {

                log.info("{} compensado com sucesso", oferta);

                publicarOfertaCompensada(lancePublicacao, oferta);

                quantidadeAlimento -= oferta.getQuantidadeAlimentoCompensado();

            }
        }
    }

    private void publicarOfertaCompensada(LancePublicacao lancePublicacao, Oferta oferta) {

        clienteConcluirLance
                .publicar(new LanceCompensado(lancePublicacao, oferta));

    }
}
