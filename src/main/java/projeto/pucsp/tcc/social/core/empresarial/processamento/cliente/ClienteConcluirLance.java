package projeto.pucsp.tcc.social.core.empresarial.processamento.cliente;

import projeto.pucsp.tcc.social.core.empresarial.processamento.modelo.LanceCompensado;

public interface ClienteConcluirLance {

    void publicar(LanceCompensado lanceCompensado);

}
