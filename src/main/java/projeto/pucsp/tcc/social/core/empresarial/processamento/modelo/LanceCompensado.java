package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class LanceCompensado {

    private final Integer lanceId;

    private final Integer ofertaId;

    private final Integer socialId;

    private final Integer empresarialId;

    private final Integer alimentoId;

    private final Integer quantidadeAlimento;

    public LanceCompensado(LancePublicacao lancePublicacao, Oferta oferta) {

        lanceId = lancePublicacao.getLanceId();

        ofertaId = oferta.getId();

        socialId = lancePublicacao.getSocialId();

        empresarialId = oferta.getEmpresaFk();

        alimentoId = lancePublicacao.getAlimentoId();

        quantidadeAlimento = oferta.getQuantidadeAlimentoCompensado();

    }
}
