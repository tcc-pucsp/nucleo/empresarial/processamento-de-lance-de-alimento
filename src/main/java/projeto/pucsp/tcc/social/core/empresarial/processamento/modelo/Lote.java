package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "lote")
@Data
public class Lote {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "situacao_lote")
    private Integer situacaoLote;

    @Column(name = "alimento_id")
    private Integer alimentoId;

    @Column(name = "numero_lote")
    private String numeroLote;

    @Column(name = "data_validade")
    private LocalDate dataValidade;

    @Column(name = "data_fabricacao")
    private LocalDate dataFabricacao;

}
