package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@Table(name = "endereco")
@Data
public class Endereco {

    @Id
    @Column(name = "id")
    private Integer id;

    @Column(name = "latitude")
    private BigDecimal latitude;

    @Column(name = "longitude")
    private BigDecimal longitude;

}
