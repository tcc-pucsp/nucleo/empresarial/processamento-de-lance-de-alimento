package projeto.pucsp.tcc.social.core.empresarial.processamento.modelo;

import lombok.Data;

@Data
public class LancePublicacao {

    private Integer lanceId;

    private Integer socialId;

    private Integer alimentoId;

    private Integer quantidadeAlimento;

    private Localizacao localizacao;

}
